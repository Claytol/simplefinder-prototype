# SimpleFinder Prototype 1.0
- This project is a prototype for BGSU's The Hatch Program - 2021
- Copywrite and ownership goes to Nathaniel Slemmons
    - Sticker represents Peripheral, Remote represents Central

## The Hatch 2021 - Results Video(1:39:54)
### https://www.youtube.com/watch?v=kqApRZk9ljE 

## Installations - Arduino Software(IDE) 
### https://www.arduino.cc/en/software

## Remote Circuits
- Arduino Nano 33 BLE(1)
- 400Tie-Points Breadboard(1)
- Power Supply Module 3.3V 5V MB102(1)
- 9v Battery Clip with 2.1mm X 5.5mm Male DC Plug(1)
- 9V Battery(1)
- Jumper Wire(2)

## Sticker Circuits
- Arduino Nano 33 BLE(1)
- 400Tie-Points Breadboard(1)
- Power Supply Module 3.3V 5V MB102(1)
- 9v Battery Clip with 2.1mm X 5.5mm Male DC Plug(1)
- 9V Battery(1)
- Jumper Wire(5)
- 2mm Mini Vibrating Disk Motor(1)
- Active or Passive Buzzer(1)
- NPN Transistor(1)
- 1K Resistor(1)
- 100Ω Resistor(1)
- LED Light(1)
